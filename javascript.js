

let firstNumber = prompt('Введите первое число');
while (isNaN(firstNumber) || !firstNumber || !firstNumber.trim()) {
  firstNumber = prompt('Введите первое число снова', firstNumber);
  }
firstNumber = +firstNumber;

let secondNumber = prompt('Введите второе число');
while (isNaN(secondNumber) || !secondNumber || !secondNumber.trim()) {
  secondNumber = prompt('Введите второе число', secondNumber);
}
secondNumber = +secondNumber;

// если просто ввести буквы - то проходит, потому что строка
let mathOperator = prompt('Введите знак операции: +, -, *, /');
while (mathOperator == null || !mathOperator || (mathOperator !== "+" && mathOperator !== "-" && mathOperator !== "*" && mathOperator !== "/")) {
  mathOperator = prompt('Введите знак операции снова: +, -, *, /');
  }

function calcResult(firstNum, secondNum, operator) {
  switch (operator) {
    case '+':return firstNum + secondNum;
    case '-':return firstNum - secondNum;
    case '*':return firstNum * secondNum;
    case '/':return firstNum / secondNum;
  }
}
console.log(calcResult(firstNumber, secondNumber, mathOperator));





